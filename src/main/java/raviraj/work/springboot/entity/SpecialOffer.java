package raviraj.work.springboot.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Special_Offer")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "specialOfferId")
public class SpecialOffer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Special_Offer_Id", nullable = false)
	private Long specialOfferId;
	
	@Column(name = "Name", nullable = false)
	private String name;
	
	@Column(name = "Fixed_Percentage_Discount", nullable = false)
	private Float fixedPercentageDiscount;
	
	@OneToMany(mappedBy = "specialOffer", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.DETACH,
			CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
//	@JsonManagedReference
	private Set<DiscountCode> discountCodes;
	
	public SpecialOffer() {
		
	}
	
	public SpecialOffer(String name, Float fixedPercentageDiscount, Set<DiscountCode> discountCodes) {
		this.discountCodes = discountCodes;
		this.name = name;
		this.fixedPercentageDiscount = fixedPercentageDiscount;
	}

	public Long getSpecialOfferId() {
		return specialOfferId;
	}

	public void setSpecialOfferId(Long specialOfferId) {
		this.specialOfferId = specialOfferId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getFixedPercentageDiscount() {
		return fixedPercentageDiscount;
	}

	public void setFixedPercentageDiscount(Float fixedPercentageDiscount) {
		this.fixedPercentageDiscount = fixedPercentageDiscount;
	}

	public Set<DiscountCode> getDiscountCodes() {
		return discountCodes;
	}

	public void setDiscountCodes(Set<DiscountCode> discountCodes) {
		this.discountCodes = discountCodes;
	}
	
}
