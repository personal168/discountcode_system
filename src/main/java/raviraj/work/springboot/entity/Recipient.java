package raviraj.work.springboot.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Recipient")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "recipientId")
public class Recipient {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Recipient_Id", nullable = false)
	private Long recipientId;
	
	@Column(name = "Name", nullable = false)
	private String name;
	
	@Column(name = "Email", nullable = false, unique = true)
	private String email;
	
	@OneToMany(mappedBy = "recipient", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.DETACH,
			CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
//	@JsonManagedReference
	private Set<DiscountCode> discountCodes;
	
	public Recipient() {
		
	}
	
	public Recipient(String name, String email) {
		this.name = name;
		this.email = email;
	}

	public Long getRecipientId() {
		return recipientId;
	}

	public void setRecipientId(Long recipientId) {
		this.recipientId = recipientId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<DiscountCode> getDiscountCodes() {
		return discountCodes;
	}

	public void setDiscountCodes(Set<DiscountCode> discountCodes) {
		this.discountCodes = discountCodes;
	}
	
}
