package raviraj.work.springboot.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Discount_Code")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "code")
public class DiscountCode {

	@Id
	@Column(name = "Code", nullable = false, unique = true)
	private String code;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "Recipient_Id", nullable = false)
	@JsonBackReference(value = "recipient")
	private Recipient recipient;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "Special_Offer_Id", nullable = false)
	@JsonBackReference(value = "specialOffer")
	private SpecialOffer specialOffer;

	@Column(name = "Expiration_Date", nullable = false)
	private LocalDate expirationDate;

	@Column(name = "Can_Use_Once", nullable = false)
	private Boolean canUseOnce;

	@Column(name = "Usage_Date")
	private LocalDate usageDate;

	public DiscountCode() {
		
	}
	
	public DiscountCode(String code, Recipient recipient, SpecialOffer specialOffer, LocalDate expirationDateObj, Boolean canUseOnce,
			LocalDate usageDate) {
		this.code = code;
		this.recipient = recipient;
		this.specialOffer = specialOffer;
		this.expirationDate = expirationDateObj;
		this.canUseOnce = canUseOnce;
		this.usageDate = usageDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Recipient getRecipient() {
		return recipient;
	}

	public void setRecipient(Recipient recipient) {
		this.recipient = recipient;
	}

	public SpecialOffer getSpecialOffer() {
		return specialOffer;
	}

	public void setSpecialOffer(SpecialOffer specialOffer) {
		this.specialOffer = specialOffer;
	}

	public LocalDate getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDate expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Boolean canUseOnce() {
		return canUseOnce;
	}

	public void setCanUseOnce(Boolean canUseOnce) {
		this.canUseOnce = canUseOnce;
	}

	public LocalDate getUsageDate() {
		return usageDate;
	}

	public void setUsageDate(LocalDate usageDate) {
		this.usageDate = usageDate;
	}

}
