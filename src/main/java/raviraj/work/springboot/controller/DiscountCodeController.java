package raviraj.work.springboot.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import raviraj.work.springboot.entity.DiscountCode;
import raviraj.work.springboot.entity.Recipient;
import raviraj.work.springboot.entity.SpecialOffer;
import raviraj.work.springboot.exception.InvalidInputException;
import raviraj.work.springboot.exception.NotAValidDiscountCodeException;
import raviraj.work.springboot.exception.ResourceNotFoundException;
import raviraj.work.springboot.service.DiscountCodeService;
import raviraj.work.springboot.service.RecipientService;
import raviraj.work.springboot.service.ValidationService;

/**
 * This class gets discount or list of discounts based on user input e.g. 1. For
 * each Recipient get all discounts 2. List all discounts under one special
 * offer
 * 
 * Developer has to add methods for different scenarios
 */
@RestController
@RequestMapping("api/v1/discountCode")
@CrossOrigin(origins = "http://localhost:4200")
public class DiscountCodeController {

	@Autowired
	private RecipientService recipientCrudService;

	@Autowired
	private DiscountCodeService discountCodeService;

	@Autowired
	private ValidationService validationService;

	@GetMapping("/{email}/get")
	public Map<String, String> getAllValidDiscountsForEmail(@PathVariable(value = "email") String email)
			throws ResourceNotFoundException {
		Recipient recipient = recipientCrudService.getRecipientByEmailId(email)
				.orElseThrow(() -> new ResourceNotFoundException("Recipient not found for email:" + email));
		return discountCodeService.getAllValidDiscountsForEmail(recipient);
	}

	@PutMapping("/{email}/{code}/get")
	public Float getPercentageDiscount(@PathVariable(value = "email") String email,
			@PathVariable(value = "code") String code)
			throws ResourceNotFoundException, NotAValidDiscountCodeException {

		DiscountCode discountCode = discountCodeService.getDiscountCode(code); // not found exception is handled by this
																				// method
		if (!validationService.isDiscountValid(discountCode)) {
			throw new NotAValidDiscountCodeException(
					"Discount code: " + code + " is not a valid code, either it has been expired or used");
		}

		return discountCodeService.saveUsageDate(discountCode).getSpecialOffer().getFixedPercentageDiscount();
	}
	
	@PostMapping("/{expirationDate}/generate")
	public ResponseEntity<List<DiscountCode>> generateDiscountCodeForAllRecipients(
			@PathVariable(value = "expirationDate") String expirationDate, @RequestBody SpecialOffer specialOffer)
			throws InvalidInputException {
		if (!validationService.isExpirationDateValid(expirationDate)) {
			throw new InvalidInputException("Expiration Date should be valid");
		}
		return discountCodeService.generateDiscountCodeForAllRecipients(expirationDate, specialOffer);
	}
}
