package raviraj.work.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import raviraj.work.springboot.entity.SpecialOffer;
import raviraj.work.springboot.service.SpecialOfferService;

@RestController
@RequestMapping("api/v1/discountCode/specialOffer")
@CrossOrigin(origins = "http://localhost:4200")
public class SpecialOfferController {

	@Autowired
	private SpecialOfferService specialOfferCrudService;
	
	@GetMapping("/")
	public List<SpecialOffer> getSpecialOffers() {
		return specialOfferCrudService.getSpecialOffers();
	}
}
