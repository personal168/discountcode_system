package raviraj.work.springboot.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import raviraj.work.springboot.entity.Recipient;

public interface RecipientRepository extends JpaRepository<Recipient, Long> {

	Optional<Recipient> findByEmail(String email);
}
