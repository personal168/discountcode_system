package raviraj.work.springboot.service;

import java.time.LocalDate;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import raviraj.work.springboot.entity.DiscountCode;

@Service
public class ValidationService {

	public boolean isExpirationDateValid(String expirationDate) {
		if (StringUtils.isEmpty(expirationDate)) {
			return false;
		}

		LocalDate expirationDateObj = LocalDate.parse(expirationDate);

		return isExpirationDateValid(expirationDateObj);
	}

	public boolean isExpirationDateValid(LocalDate expirationDate) {
		return LocalDate.now().isBefore(expirationDate);
	}

	public boolean isDiscountValid(DiscountCode discountCode) {
		return isExpirationDateValid(discountCode.getExpirationDate())
				&& StringUtils.isEmpty(discountCode.getUsageDate());
	}
}
