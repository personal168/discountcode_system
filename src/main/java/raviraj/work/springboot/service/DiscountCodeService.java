package raviraj.work.springboot.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raviraj.work.springboot.entity.DiscountCode;
import raviraj.work.springboot.entity.Recipient;
import raviraj.work.springboot.entity.SpecialOffer;
import raviraj.work.springboot.exception.ResourceNotFoundException;
import raviraj.work.springboot.repository.DiscountCodeRepository;

@Service
public class DiscountCodeService {

	@Autowired
	private DiscountCodeRepository discountCodeRepository;
	
	@Autowired
	private RecipientService recipientService;
	
	@Autowired
	private Random random;
	
	@Autowired
	private ValidationService validationService;
	
	public Map<String, String> getAllValidDiscountsForEmail(Recipient recipient) {
		Map<String, String> discountWithOfferName = new HashMap<String, String>();
		recipient.getDiscountCodes().forEach(discount -> {
			if(validationService.isDiscountValid(discount)) {
				discountWithOfferName.put(discount.getCode(), discount.getSpecialOffer().getName());
			}
		});
		return discountWithOfferName;
	}

	@Transactional
	public ResponseEntity<List<DiscountCode>> generateDiscountCodeForAllRecipients(String expirationDate, SpecialOffer specialOffer) {
		List<DiscountCode> discountCodes = new ArrayList<DiscountCode>();
		String prefix = specialOffer.getName().substring(0, 4);
		LocalDate expirationDateObj = LocalDate.parse(expirationDate);
		
		List<Recipient> recipients = recipientService.getAllRecipients();
		recipients.forEach(recipient -> {
			DiscountCode discountCode = new DiscountCode(generateRandomCode(prefix), recipient, specialOffer, expirationDateObj, true, null);
			discountCodes.add(discountCode);
		});
		
		List<DiscountCode> savedDiscountCodes = discountCodeRepository.saveAll(discountCodes);
		
		return ResponseEntity.ok().body(savedDiscountCodes);
	}
	
	private String generateRandomCode(String prefix) {
		int num = random.nextInt(10000);
		return prefix.concat("-SO-").concat(String.valueOf(num));
	}
	
	@Transactional(readOnly = true) // to improve read performance
	public DiscountCode getDiscountCode(String code) throws ResourceNotFoundException {
		return discountCodeRepository.findById(code).orElseThrow(() -> new ResourceNotFoundException("Discount Details not found"));
	}
	
	@Transactional
	public DiscountCode saveUsageDate(DiscountCode discountCode) {
		discountCode.setUsageDate(LocalDate.now());
		return discountCodeRepository.save(discountCode);
	}
}
