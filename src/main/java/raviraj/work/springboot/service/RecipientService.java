package raviraj.work.springboot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raviraj.work.springboot.entity.Recipient;
import raviraj.work.springboot.repository.RecipientRepository;

@Service
public class RecipientService {

	@Autowired
	private RecipientRepository recipientRepository;
	
	@Transactional(readOnly = true) // to improve read performance
	public List<Recipient> getAllRecipients() {
		return recipientRepository.findAll();
	}
	
	@Transactional(readOnly = true) // to improve read performance
	public Optional<Recipient> getRecipientByEmailId(String email) {
		return recipientRepository.findByEmail(email);
	}
}
