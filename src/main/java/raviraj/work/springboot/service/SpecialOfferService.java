package raviraj.work.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import raviraj.work.springboot.entity.SpecialOffer;
import raviraj.work.springboot.repository.SpecialOfferRepository;

@Service
public class SpecialOfferService {

	@Autowired
	private SpecialOfferRepository specialOfferRepository;
	
	@Transactional(readOnly = true) // to improve read performance
	public List<SpecialOffer> getSpecialOffers() {
		return specialOfferRepository.findAll();
	}
}
