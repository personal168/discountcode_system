package raviraj.work.springboot.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import raviraj.work.springboot.entity.DiscountCode;
import raviraj.work.springboot.entity.Recipient;
import raviraj.work.springboot.entity.SpecialOffer;
import raviraj.work.springboot.service.DiscountCodeService;
import raviraj.work.springboot.service.RecipientService;
import raviraj.work.springboot.service.ValidationService;

@WebMvcTest(value = DiscountCodeController.class)
public class DiscountCodeControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private RecipientService recipientCrudService;

	@MockBean
	private DiscountCodeService discountCodeService;

	@MockBean
	private ValidationService validationService;
	
	@Test
	public void getAllValidDiscountsForEmailTest() throws Exception {
		Map<String, String> discountCodesWithOffer = new HashMap<String, String>();
		discountCodesWithOffer.put("Summ-SO-1111", "Summer Sale");
		Recipient mockedRecipient = new Recipient();
		Optional<Recipient> optionalR = Optional.of(mockedRecipient);
		
		Mockito.when(recipientCrudService.getRecipientByEmailId(Mockito.anyString())).thenReturn(optionalR);
		Mockito.when(discountCodeService.getAllValidDiscountsForEmail(Mockito.any())).thenReturn(discountCodesWithOffer);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("https://localhost:8080/api/v1/discountCode/raj@gmail.com/get")
				.accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}
	
	@Test
	public void getAllValidDiscountsForEmailThrowsResourceNotFoundExceptionTest() throws Exception {
		String mockedEmail = "raj1@gmail.com";
		Mockito.when(recipientCrudService.getRecipientByEmailId(mockedEmail)).thenCallRealMethod();

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("https://localhost:8080/api/v1/discountCode/raj@gmail.com/get")
				.accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(404, response.getStatus());
	}
	
	@Test
	public void getPercentageDiscountTest() throws Exception {
		String mockedExpirationDate = "2050-10-10";

		DiscountCode discountCode = new DiscountCode();
		SpecialOffer specialOffer = new SpecialOffer();
		discountCode.setSpecialOffer(specialOffer);
		Mockito.when(discountCodeService.getDiscountCode(Mockito.anyString())).thenReturn(discountCode);
		Mockito.when(validationService.isDiscountValid(discountCode)).thenReturn(true);
		Mockito.when(discountCodeService.saveUsageDate(discountCode)).thenReturn(discountCode);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("https://localhost:8080/api/v1/discountCode/raj@gmail.com/Summ-SO-1111/get")
				.accept(MediaType.APPLICATION_JSON).content(mockedExpirationDate).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(200, response.getStatus());
	}
	
	@Test
	public void getPercentageDiscountThrowsNotAValidDiscountCodeExceptionTest() throws Exception {
		String mockedExpirationDate = "2050-10-10";

		DiscountCode discountCode = new DiscountCode();
		SpecialOffer specialOffer = new SpecialOffer();
		discountCode.setSpecialOffer(specialOffer);

		Mockito.when(discountCodeService.getDiscountCode(Mockito.anyString())).thenReturn(null);
		Mockito.when(validationService.isDiscountValid(Mockito.any(DiscountCode.class))).thenReturn(false);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("https://localhost:8080/api/v1/discountCode/raj@gmail.com/Summ-SO-1111/get")
				.accept(MediaType.APPLICATION_JSON).content(mockedExpirationDate).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertTrue(response.getContentAsString().contains("is not a valid code, either it has been expired or used"));
	}
	
	@Test
	public void generateDiscountCodeForAllRecipientsTest() throws Exception {
		String mockedSpecialOfferJson = "{\r\n" + 
				"    \"specialOfferId\":12,\r\n" + 
				"    \"name\": \"Summer Sale\",\r\n" + 
				"    \"fixedPercentageDiscount\": 4.1\r\n" + 
				"}";

		DiscountCode discountCode = new DiscountCode();
		SpecialOffer specialOffer = new SpecialOffer();
		discountCode.setSpecialOffer(specialOffer);
		List<DiscountCode> discountCodes = new ArrayList<DiscountCode>();
		ResponseEntity<List<DiscountCode>> responseEntity = ResponseEntity.ok().body(discountCodes);
		Mockito.when(validationService.isExpirationDateValid(Mockito.anyString())).thenReturn(true);
		Mockito.when(discountCodeService.generateDiscountCodeForAllRecipients(Mockito.anyString(), Mockito.any(SpecialOffer.class))).thenReturn(responseEntity);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("https://localhost:8080/api/v1/discountCode/2050-10-10/generate")
				.accept(MediaType.APPLICATION_JSON).content(mockedSpecialOfferJson).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(200, response.getStatus());
	}
	
	@Test
	public void generateDiscountCodeForAllRecipientsInvalidInputExceptionTest() throws Exception {
		String mockedSpecialOfferJson = "{\r\n" + 
				"    \"specialOfferId\":12,\r\n" + 
				"    \"name\": \"Summer Sale\",\r\n" + 
				"    \"fixedPercentageDiscount\": 4.1\r\n" + 
				"}";

		DiscountCode discountCode = new DiscountCode();
		SpecialOffer specialOffer = new SpecialOffer();
		discountCode.setSpecialOffer(specialOffer);
		
		Mockito.when(validationService.isExpirationDateValid(Mockito.anyString())).thenReturn(false);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("https://localhost:8080/api/v1/discountCode/2050-10-10/generate")
				.accept(MediaType.APPLICATION_JSON).content(mockedSpecialOfferJson).contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(400, response.getStatus());
	}
}
