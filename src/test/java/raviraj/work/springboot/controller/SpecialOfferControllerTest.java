package raviraj.work.springboot.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import raviraj.work.springboot.entity.SpecialOffer;
import raviraj.work.springboot.service.SpecialOfferService;

@WebMvcTest(value = SpecialOfferController.class)
public class SpecialOfferControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private SpecialOfferService specialOfferCrudService;
	
//	@Test
	public void getSpecialOffersTest() throws Exception {
		List<SpecialOffer> specialOffers = new ArrayList<SpecialOffer>();
		specialOffers.add(new SpecialOffer());
		
		Mockito.when(specialOfferCrudService.getSpecialOffers()).thenReturn(specialOffers);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("https://localhost:8080/api/v1/discountCode/specialOffer")
				.accept(MediaType.APPLICATION_JSON).characterEncoding("application/json").contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}
}
