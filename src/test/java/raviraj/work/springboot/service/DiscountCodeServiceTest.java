package raviraj.work.springboot.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

import raviraj.work.springboot.entity.DiscountCode;
import raviraj.work.springboot.entity.Recipient;
import raviraj.work.springboot.entity.SpecialOffer;
import raviraj.work.springboot.exception.ResourceNotFoundException;
import raviraj.work.springboot.repository.DiscountCodeRepository;


@WebMvcTest(value = DiscountCodeService.class)
public class DiscountCodeServiceTest {

	@SpyBean
	private DiscountCodeService discountCodeService;
	
	@MockBean
	private DiscountCodeRepository discountCodeRepository;
	
	@MockBean
	private RecipientService recipientService;
	
	@MockBean
	private Random random;
	
	@SpyBean
	private ValidationService validationService;
	
	@Test
	public void getAllValidDiscountsForEmailTest() throws Exception {
		Recipient recipient = new Recipient();
		DiscountCode discountCode1 = new DiscountCode("Summ-SO-1111", recipient, new SpecialOffer(), LocalDate.of(2050, 10, 10), true, null);
		DiscountCode discountCode2 = new DiscountCode("Summ-SO-1112", recipient, new SpecialOffer(), LocalDate.of(2050, 10, 10), true, null);
		
		Set<DiscountCode> set = new HashSet<DiscountCode>();
		set.add(discountCode1);
		set.add(discountCode2);
		
		recipient.setDiscountCodes(set);
		
		List<SpecialOffer> specialOffers = new ArrayList<SpecialOffer>();
		specialOffers.add(new SpecialOffer());
		
		Map<String, String> map = discountCodeService.getAllValidDiscountsForEmail(recipient);
		assertTrue(map.size() > 0);
		Mockito.verify(validationService, new Times(2)).isDiscountValid(Mockito.any(DiscountCode.class));
	}
	
	@Test
	public void getAllValidDiscountsForEmailReturnsEmptyMapTest() throws Exception {
		Recipient recipient = new Recipient();
		
		DiscountCode discountCode1 = new DiscountCode("Summ-SO-1111", recipient, new SpecialOffer(), LocalDate.of(2019, 10, 10), true, null);
		DiscountCode discountCode2 = new DiscountCode("Summ-SO-1112", recipient, new SpecialOffer(), LocalDate.of(2019, 10, 10), true, null);
		
		Set<DiscountCode> set = new HashSet<DiscountCode>();
		set.add(discountCode1);
		set.add(discountCode2);
		
		recipient.setDiscountCodes(set);
		
		List<SpecialOffer> specialOffers = new ArrayList<SpecialOffer>();
		specialOffers.add(new SpecialOffer());
		
		Map<String, String> map = discountCodeService.getAllValidDiscountsForEmail(recipient);
		assertTrue(map.size() == 0);
		Mockito.verify(validationService, new Times(2)).isDiscountValid(Mockito.any(DiscountCode.class));
	}
	
	@Test
	public void getDiscountCodeTest() throws ResourceNotFoundException {
		String code = "Summ-SO-1111";
		DiscountCode discountCode = new DiscountCode(code, null, null, LocalDate.of(2050, 10, 10), true, null);
		Optional<DiscountCode> optionalDS = Optional.of(discountCode);
		Mockito.when(discountCodeRepository.findById(code)).thenReturn(optionalDS);
		DiscountCode returned = discountCodeService.getDiscountCode(code);
		assertEquals("Summ-SO-1111", returned.getCode());
		Mockito.verify(discountCodeRepository).findById(code);
	}
	
	@Test
	public void getDiscountCodeThrowsResourceNotFoundExceptionTest() throws ResourceNotFoundException {
		String code = "Summ-SO-1111";
		DiscountCode discountCode = new DiscountCode("", null, null, LocalDate.of(2050, 10, 10), true, null);
		Optional<DiscountCode> optionalDS = Optional.of(discountCode);
		Mockito.when(discountCodeRepository.findById(code)).thenReturn(optionalDS);
		DiscountCode returned = discountCodeService.getDiscountCode(code);
		assertNotEquals("Summ-SO-1111", returned.getCode());
		Mockito.verify(discountCodeRepository).findById(code);
	}
	
	@Test
	public void saveUsageDateTest() {
		String code = "Summ-SO-1111";
		DiscountCode discountCode = new DiscountCode(code, null, null, LocalDate.of(2050, 10, 10), true, null);
		Mockito.when(discountCodeRepository.save(discountCode)).thenReturn(discountCode);
		DiscountCode returned = discountCodeService.saveUsageDate(discountCode);
		assertEquals("Summ-SO-1111", returned.getCode());
		assertEquals(LocalDate.now(), returned.getUsageDate());
		Mockito.verify(discountCodeRepository).save(discountCode);
	}
}
