package raviraj.work.springboot.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

import raviraj.work.springboot.entity.Recipient;
import raviraj.work.springboot.entity.SpecialOffer;
import raviraj.work.springboot.repository.SpecialOfferRepository;


@WebMvcTest(value = SpecialOfferService.class)
public class SpecialOfferServiceTest {

	@SpyBean
	private SpecialOfferService specialOfferService;
	
	@MockBean
	private SpecialOfferRepository specialOfferRepository;
	
	@Test
	public void getSpecialOffersReturnsListOfSpecialOffersTest() {
		List<SpecialOffer> specialOffers = new ArrayList<SpecialOffer>();
		specialOffers.add(new SpecialOffer());
		Mockito.when(specialOfferRepository.findAll()).thenReturn(specialOffers);
		List<SpecialOffer> returnedList = specialOfferService.getSpecialOffers();
		assertTrue(returnedList.size() > 0);
		Mockito.verify(specialOfferRepository).findAll();
	}
	
	@Test
	public void getSpecialOffersReturnsEmptyListTest() {
		List<SpecialOffer> specialOffers = new ArrayList<SpecialOffer>();
		Mockito.when(specialOfferRepository.findAll()).thenReturn(specialOffers);
		List<SpecialOffer> returnedList = specialOfferService.getSpecialOffers();
		assertFalse(returnedList.size() > 0);
		Mockito.verify(specialOfferRepository).findAll();
	}
}
