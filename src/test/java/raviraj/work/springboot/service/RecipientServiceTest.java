package raviraj.work.springboot.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

import raviraj.work.springboot.entity.Recipient;
import raviraj.work.springboot.repository.RecipientRepository;

@WebMvcTest(value = RecipientService.class)
public class RecipientServiceTest {

	@SpyBean
	private RecipientService recipientService;
	
	@MockBean
	private RecipientRepository recipientRepository;
	
	@Test
	public void getAllRecipientsReturnsListOfRecipientsTest() {
		List<Recipient> recipients = new ArrayList<Recipient>();
		recipients.add(new Recipient());
		Mockito.when(recipientRepository.findAll()).thenReturn(recipients);
		List<Recipient> returnedList = recipientService.getAllRecipients();
		assertTrue(returnedList.size() > 0);
		Mockito.verify(recipientRepository).findAll();
	}
	
	@Test
	public void getAllRecipientsReturnsEmptyListTest() {
		List<Recipient> recipients = new ArrayList<Recipient>();
		Mockito.when(recipientRepository.findAll()).thenReturn(recipients);
		List<Recipient> returnedList = recipientService.getAllRecipients();
		assertFalse(returnedList.size() > 0);
		Mockito.verify(recipientRepository).findAll();
	}
	
	@Test
	public void getRecipientByEmailIdReturnsRecipientTest() {
		String email = "raj@gmail.com";
		Recipient recipient = new Recipient();
		Optional<Recipient> optionalRecipient = Optional.of(recipient);
		Mockito.when(recipientRepository.findByEmail(email)).thenReturn(optionalRecipient);
		Optional<Recipient> returned = recipientService.getRecipientByEmailId(email);
		assertTrue(returned.isPresent());
		Mockito.verify(recipientRepository).findByEmail(email);
	}
	
	@Test
	public void getRecipientByEmailIdReturnsNoRecipientTest() {
		String email = "raj@gmail.com";
		Mockito.when(recipientRepository.findByEmail(email)).thenReturn(Optional.empty());
		Optional<Recipient> returned = recipientService.getRecipientByEmailId(email);
		assertFalse(returned.isPresent());
		Mockito.verify(recipientRepository).findByEmail(email);
	}
}
