package raviraj.work.springboot.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import raviraj.work.springboot.entity.DiscountCode;
import raviraj.work.springboot.entity.Recipient;
import raviraj.work.springboot.entity.SpecialOffer;

@WebMvcTest(value = ValidationService.class)
public class ValidationServiceTest {

	@SpyBean
	private ValidationService validationService;
	
	@Test
	public void isExpirationDateValidReturnsTrueTest() {
		assertTrue(validationService.isExpirationDateValid("2050-10-10"));
	}
	
	@Test
	public void isExpirationDateValidReturnsFalseTest() {
		assertFalse(validationService.isExpirationDateValid("2019-10-10"));
	}
	
	@Test
	public void isExpirationDateValidReturnsFalseIfDateIsEmptyTest() {
		assertFalse(validationService.isExpirationDateValid(""));
	}
	
	@Test
	public void isExpirationDateValidReturnsTrueIfDateIsAfterTodaysDateTest() {
		assertTrue(validationService.isExpirationDateValid(LocalDate.of(2050, 10, 10)));
	}
	
	@Test
	public void isExpirationDateValidReturnsFalseIfDateIsBeforeTodaysDateTest() {
		assertFalse(validationService.isExpirationDateValid(LocalDate.of(2019, 10, 10)));
	}
	
	@Test
	public void isDiscountValidReturnsTrueTest() {
		DiscountCode discountCode = new DiscountCode("Summ-SO-1111", new Recipient(), new SpecialOffer(), LocalDate.of(2050, 10, 10), true, null);
		assertTrue(validationService.isDiscountValid(discountCode));
	}
	
	@Test
	public void isDiscountValidReturnsFalseIfExpirationDateIsBeforeTodaysDateTest() {
		DiscountCode discountCode = new DiscountCode("Summ-SO-1111", new Recipient(), new SpecialOffer(), LocalDate.of(2019, 10, 10), true, null);
		assertFalse(validationService.isDiscountValid(discountCode));
	}
	
	@Test
	public void isDiscountValidReturnsFalseIfUsageDateIsNotEmptyTest() {
		DiscountCode discountCode = new DiscountCode("Summ-SO-1111", new Recipient(), new SpecialOffer(), LocalDate.of(2050, 10, 10), true, LocalDate.of(2019, 10, 10));
		assertFalse(validationService.isDiscountValid(discountCode));
	}
}
